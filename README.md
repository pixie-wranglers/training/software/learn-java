# Learn Java for FRC

This repository is intended to help new students understand the basics of Java
with the goal of using it in FRC.

It is broken up into three major areas:

- [Required Knowledge](0-required)
  - These concepts are necessary in order to build effective robot code.
- [Useful Knowledge](1-useful)
  - These concepts are will help elevate your robot code and make it more effective.
- [Extra Knowledge](2-extra)
  - These concepts don't typically come up in FRC work, but may be of interest
  if you are looking for some more depth.

It is highly recommended that you complete these in order as each will build
off of the previous sections.

## Contributors

### 3786 Charger Robotics

[![3786_logo]](https://chargerrobotics.com)

|  |  |
| ---- | ---- |
| Code Repositories | [![gitlab_logo_small]](https://gitlab.com/chargerrobotics3786)|

If you would like to contribute, see [CONTRIBUTING.md](CONTRIBUTING.md)

<!-- Image References -->
<!-- Small Logo images should be 25px square,
       larger team logos should be 100px in height-->
<!-- markdownlint-disable MD053 -->
[gitlab_logo_small]: resources/gitlab-logo-small.png "GitLab"
[3786_logo]: resources/3786-logo.png "3786 Charger Robotics"
